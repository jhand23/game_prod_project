using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectables : MonoBehaviour
{
    public GameObject runText;

    private int tokenyes;


    // Start is called before the first frame update
    void Start()
    {
        runText.SetActive(false);
        tokenyes = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "JohnLemon")
        {
            other.GetComponent<PlayerMovement>().points++;
            Destroy(gameObject);

            tokenyes = tokenyes + 1;
        }

        if (tokenyes == 1)
        {
            runText.SetActive(true);
        }
    }
}
