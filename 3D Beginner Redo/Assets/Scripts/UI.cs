using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    public GameObject introText;
    
    // Start is called before the first frame update
    void Start()
    {
        introText.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Submit"))
            introText.SetActive(false);

    }
}
